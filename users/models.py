from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.management.utils import get_random_secret_key
from django.db import models


# User Manager
class UserManager(BaseUserManager):
    def create_user(self, username, password=None, **extra_fields):
        if not username:
            raise ValueError('Please, type username')
        user = self.model(
            username=username,
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        user = self.create_user(
            username=username,
            password=password,
            **extra_fields
        )

        if extra_fields.get('is_staff') is not True:
            raise ValueError('is_staff=True required for Superuser')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('is_superuser=True required for Superuser')

        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    # Логин
    username = models.CharField(
        verbose_name='Логин пользователя',
        max_length=128,
        unique=True,
        db_index=True
    )
    # Имя
    first_name = models.CharField(
        verbose_name='Имя',
        max_length=100,
        null=True,
        blank=True
    )
    # Фамилия
    last_name = models.CharField(
        verbose_name='Фамилия',
        max_length=100,
        null=True,
        blank=True
    )
    secret_key = models.CharField(max_length=255, default=get_random_secret_key)

    # number
    number = models.CharField(
        verbose_name='телефон номер',
        max_length=13,
        null=True,
        blank=True
    )
    # email
    email = models.CharField(
        verbose_name='почта',
        max_length=15,
        null=True,
        blank=True
    )
    # Дата создания
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )

    USERNAME_FIELD = 'username'
    objects = UserManager()

    # Django Additional (не трогать)
    is_staff = models.BooleanField(
        default=False,
        verbose_name='Сотрудник организации',
        help_text='Открывает доступ к панели администратора с указанными правами',
    )  # права к админ-панели
    is_active = models.BooleanField(
        default=True,
        verbose_name='Активный аккаунт',
        help_text='Статус (по умолчанию "Да")',
    )


    class Meta:

        # swappable = 'AUTH_USER_MODEL'

        verbose_name = 'Пользователь системы'
        verbose_name_plural = 'Пользователи системы'

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'.strip()

