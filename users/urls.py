from django.urls import path
from users import views

urlpatterns = [
    path('registration/', views.user_register, name="reg"),
    # path('login/', views.user_login, name="log"),
    path('me/', views.me, name="me"),
]
