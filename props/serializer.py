from rest_framework import serializers

from props.models import Request


class ReqSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = '__all__'

