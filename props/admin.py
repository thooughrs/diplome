from django.contrib import admin

from props.models import Request, CallBook


# Register your models here.
class RequestAdmin(admin.ModelAdmin):
    model = Request
    list_display = (
        'company_name',
        'position',
        'aims',
        'work_area',
        'employee_count',
        'info_type',
        'system_complexity',
        'law_duty',
        'risk_level',
        'deadline',
        'tarif',
    )
    list_filter = (
        'company_name',
    )
    search_fields = (
        "company_name",
    )


class CallBookAdmin(admin.ModelAdmin):
    model = CallBook
    list_display = (
        'first_name',
        'last_name',
        'number',
        'email',
        'text',
        'created_at',
    )
    list_filter = (
        'email',
    )
    search_fields = (
        "email",
    )


admin.site.register(CallBook, CallBookAdmin)
admin.site.register(Request, RequestAdmin)
