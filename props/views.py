from datetime import datetime

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from props.models import Request, CallBook
from props.serializer import ReqSerializer


# Create your views here.
@api_view(['POST'])
@permission_classes((AllowAny,))
def request(request):
    if request.data['companyName']:
        companyName = request.data['companyName']
    else:
        return Response({'message': 'companyName not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['position']:
        position = request.data['position']
    else:
        return Response({'message': 'position not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['aims']:
        aims = request.data['aims']
    else:
        return Response({'message': 'aims not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['workArea']:
        workArea = request.data['workArea']
    else:
        return Response({'message': 'workArea not found'}, status=status.HTTP_400_BAD_REQUEST)

    if request.data['employeeCount']:
        employeeCount = request.data['employeeCount']
    else:
        return Response({'message': 'employeeCount not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['infoType']:
        infoType = request.data['infoType']
    else:
        return Response({'message': 'infoType not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['systemComplexity']:
        systemComplexity = request.data['systemComplexity']
    else:
        return Response({'message': 'systemComplexity not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['lawDuty']:
        law_duty = request.data['lawDuty']
    else:
        return Response({'message': 'law_duty not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['riskLevel']:
        risk_level = request.data['riskLevel']
    else:
        return Response({'message': 'law_duty not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['deadline']:
        deadline = request.data['deadline']
    else:
        return Response({'message': 'deadline not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['comment']:
        comment = request.data['comment']
    else:
        return Response({'message': 'comment not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['tarif']:
        tarif = request.data['tarif']
    else:
        return Response({'message': 'tarif not found'}, status=status.HTTP_400_BAD_REQUEST)
    request = Request(
        user=request.user,
        company_name=companyName,
        position=position,
        aims=aims,
        work_area=workArea,
        employee_count=employeeCount,
        info_type=infoType,
        system_complexity=systemComplexity,
        law_duty=law_duty,
        risk_level=risk_level,
        deadline=datetime.strptime(deadline, '%d.%m.%Y'),
        comment=comment,
        tarif=tarif,
    )
    request.save()
    return Response({"status": 'принято'}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def call(request):
    if request.data['first_name']:
        first_name = request.data['first_name']
    else:
        return Response({'message': 'first_name not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['last_name']:
        last_name = request.data['last_name']
    else:
        return Response({'message': 'last_name not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['number']:
        number = request.data['number']
    else:
        return Response({'message': 'number not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['email']:
        email = request.data['email']
    else:
        return Response({'message': 'email not found'}, status=status.HTTP_400_BAD_REQUEST)
    if request.data['text']:
        text = request.data['text']
    else:
        return Response({'message': 'text not found'}, status=status.HTTP_400_BAD_REQUEST)

    call = CallBook(
        first_name=first_name,
        last_name=last_name,
        number=number,
        email=email,
        text=text,
    )
    call.save()
    return Response({"status": 'принято'}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def request_get(request):
    user = request.user
    request = Request.objects.filter(user=user).all()
    req = ReqSerializer(request, many=True)

    return Response(req.data, status=status.HTTP_200_OK)