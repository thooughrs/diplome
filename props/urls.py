from django.urls import path
from props import views

urlpatterns = [
    path('request/', views.request, name="reg"),
    path('request_get/', views.request_get, name="request_get"),
    path('call/', views.call, name="log"),

]
