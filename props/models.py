from django.db import models

from users.models import User


class CallBook(models.Model):
    first_name = models.CharField(
        max_length=20,
        verbose_name='имя',
        db_index=True,
    )
    last_name = models.CharField(
        max_length=20,
        verbose_name='фамилия',
        db_index=True,
    )
    number = models.CharField(
        max_length=20,
        verbose_name='номер',
        db_index=True,
    )
    email = models.CharField(
            max_length=20,
            verbose_name='почта',
            db_index=True,
        )
    text = models.CharField(
            max_length=250,
            verbose_name='сообщение',
            db_index=True,
        )

    # Дата создания
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )

    class Meta:
        verbose_name = 'Book a Call'
        verbose_name_plural = 'Book a Call'

    def __str__(self):
        return self.email


class Request(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='User',
        default=''
    )
    company_name = models.CharField(
        max_length=200,
        verbose_name='компания',
        default=''
    )
    position = models.CharField(
        max_length=200,
        verbose_name='position',
        default=''
    )
    aims = models.CharField(
        max_length=200,
        verbose_name='aims',
        default=''
    )
    work_area = models.CharField(
        max_length=200,
        verbose_name='work_area',
        default='',
    )
    employee_count = models.CharField(
        max_length=200,
        verbose_name='employee_count',
        default=''
    )
    info_type = models.CharField(
        max_length=200,
        verbose_name='info_type',
        default=''
    )
    system_complexity = models.IntegerField(
        verbose_name='system_complexity',
        default=0
    )
    law_duty = models.CharField(
        max_length=200,
        verbose_name='law_duty',
        default=''
    )
    risk_level = models.IntegerField(
        verbose_name='risk_level',
        default=0
    )
    deadline = models.DateTimeField(
        max_length=200,
        verbose_name='deadline',
        null=True
    )
    comment = models.CharField(
        max_length=200,
        verbose_name='comment',
        default=''
    )
    tarif = models.CharField(
        max_length=200,
        verbose_name='tarif',
        default=''
    )
    status_choice = (
        ('Готово', "Готово"),
        ('Ожидает', "Ожидает"),
    )
    status = models.CharField(
        choices=status_choice,
        default='Ожидает',
        max_length=20,
        verbose_name="status"

    )

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявка'

    def __str__(self):
        return self.company_name
