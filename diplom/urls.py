from django.urls import path, include

from django.contrib import admin
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

import props
import users

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include('users.urls')),
    path('prop/', include('props.urls')),
    path('auth/', obtain_auth_token, name='api_token_auth'),

]
